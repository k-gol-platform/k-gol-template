#!/bin/sh
export KUBECONFIG=../kubeconfig.yaml

export TAG=$(git rev-parse --short HEAD)
export BRANCH=$(git symbolic-ref --short HEAD)

export NAMESPACE="kgol"

kubectl describe namespace ${NAMESPACE} 
exit_code=$?
if [[ exit_code -eq 1 ]]; then
  echo "🖐️ ${NAMESPACE} does not exist"
  echo "⏳ Creating the namespace..."
  kubectl create namespace ${NAMESPACE}
else 
  echo "👋 ${NAMESPACE} already exists"
fi

kubectl create -f debug.pod.yaml

echo "use: kubectl exec -n kgol -it kgol-debug -- /bin/bash"