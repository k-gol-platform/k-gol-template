# Contributing

## I want to contribute!

- Always start with an issue (use the issue templates when it's possible)
- All **merge requests** must be created from an issue (requirements traceability)
- Wait for the ~"merge request candidate" label if you propose a new feature, a bug fix, documentation update, etc...

## Contribution Flow

- Create an issue (use the issue templates when it's possible)
- Add appropriate labels to the issue
- Wait for the ~"merge request candidate" label
- Create the merge request from the issue
- Start coding
  - 👋 use [https://gitmoji.carloscuesta.me/](https://gitmoji.carloscuesta.me/) for the commit messages
- Ask for a review (or several reviews)

## Code of Conduct

- Be nice and respectful
- Help the others