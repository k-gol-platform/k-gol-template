#!/bin/sh
# delete previous namespace if exist

# commit your changes before
git add .
git commit -m "👋 deploy function"

# 🖐️ change these 2 variables
export KUBECONFIG=./kubeconfig.yaml
export DOMAIN="palavas-cluster.k3s.nimbo"

export APPLICATION_NAME=$(basename $(git rev-parse --show-toplevel))
export TAG=$(git rev-parse --short HEAD)
export BRANCH=$(git symbolic-ref --short HEAD)

#export NAMESPACE="kgol-${BRANCH}-${TAG}"
export NAMESPACE="kgol"

kubectl describe namespace ${NAMESPACE} 
exit_code=$?
if [[ exit_code -eq 1 ]]; then
  echo "🖐️ ${NAMESPACE} does not exist"
  echo "⏳ Creating the namespace..."
  kubectl create namespace ${NAMESPACE}
else 
  echo "👋 ${NAMESPACE} already exists"
fi

if [[ -z "${CI_PROJECT_PATH_SLUG}" ]]; then
  export CI_PROJECT_PATH_SLUG="only_with_gitlab_ci"
fi

if [[ -z "${CI_ENVIRONMENT_SLUG}" ]]; then
  export CI_ENVIRONMENT_SLUG="only_with_gitlab_ci"
fi

export CONTAINER_PORT=${CONTAINER_PORT:-8080}
export EXPOSED_PORT=${EXPOSED_PORT:-80}


export HOST="${APPLICATION_NAME}.${BRANCH}.${DOMAIN}"

envsubst < ./deploy.template.yaml > ./tmp/deploy.${TAG}.yaml
#kubectl apply -f ./tmp/deploy.${TAG}.yaml -n ${NAMESPACE}
#kubectl create configmap ${APPLICATION_NAME}-${TAG}-golo-functions -n ${NAMESPACE} --from-file goloFunc.golo 
 
kubectl create configmap ${APPLICATION_NAME}-${TAG}-golo-functions --from-file goloFunc.golo -o yaml --dry-run | kubectl apply -n ${NAMESPACE} -f -
kubectl apply -f ./tmp/deploy.${TAG}.yaml -n ${NAMESPACE}

kubectl get pods --namespace ${NAMESPACE}
echo "🌍 http://${HOST}/exec"

echo "to delete the pod: kubectl delete namespace ${NAMESPACE}"
